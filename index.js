const http = require("http")

const expressServer = require("./src/express/application")
const graphqlServer = require("./src/apollo")

const { port } = require("./src/settings")


const httpServer = http.createServer(expressServer)

graphqlServer.installSubscriptionHandlers(httpServer)

httpServer.listen({ port: port }, () => {
	console.log('http://localhost:' + port + graphqlServer.graphqlPath)
	console.log('ws://localhost:' + port + graphqlServer.subscriptionsPath)
})
