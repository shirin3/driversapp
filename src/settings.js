require('dotenv').config()

module.exports = {
    port: process.env.PORT,
	graphqlPath: '/graphql',
	host: process.env.DB_HOST,
	database: process.env.DB_DATABASE,
	user: process.env.DB_USER,
	password: process.env.DB_PASSWORD,
	portDb: process.env.DB_PORT,
	SECRET_KEY: process.env.SECRET_KEY
}