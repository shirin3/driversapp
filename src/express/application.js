const express = require("express")

// General routes
const General = require("./modules/general/general")

const app = express()

app.use(express.json())

app.use(General)

module.exports = app
