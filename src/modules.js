const drivers = require("./modules/drivers/index");
const states = require("./modules/states/index");
const admins = require("./modules/admins/index");
const contacts = require("./modules/contacts/index");

module.exports = [
	drivers,
	states,
	admins,
	contacts
]
