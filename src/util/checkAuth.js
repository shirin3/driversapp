const {
    verify
} = require("jsonwebtoken");
const {
    fetchOne
} = require("../library/database/postgres");
const {
    AuthenticationError
} = require("apollo-server-express")
const { SECRET_KEY } = require("../settings")


module.exports = async (context) => {
    const token = context.req.headers.authorization
    
    if(token) {
        try {
            const user = verify(token, SECRET_KEY)
            if(user) {
                const admin = await fetchOne('select * from admins where id = $1', user.id)
                if(admin){
                    return admin
                } else {
                    throw new AuthenticationError('Admin is not found')
                }
            }
        } catch (error) {
            throw new AuthenticationError(error.message)
        }
    }
    throw new Error('Authorization header must provided')
    
}