// express server
const app = require("./express/application")

// graphql service
const { ApolloServer } = require("apollo-server-express")

// load graphql modules
const modules = require("./modules.js")

// load graphql path
const { graphqlPath } = require("./settings")

// create graphql server
const server = new ApolloServer({
	modules,
	context : ({req , res , connection }) => ({ req }),
	subscriptions: {
		onConnect: (connectionParams, webSocket, context) => {},
		onDisconnect: (webSocket, context) => {},
	},
	introspection: true,
  	playground: true
})

// apply middleware
server.applyMiddleware({ app, path: graphqlPath })

module.exports = server