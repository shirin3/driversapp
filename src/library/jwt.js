const {sign, verify} = require('jsonwebtoken')
const {secretKey} = require('../settings')
module.exports = {
  sign : async (payload, option) => sign(payload, secretKey, option),
  verify : async (token, option) => verify(token, secretKey, option)
}