const { fetch, fetchOne } = require('../../library/database/postgres')
const contactQuery = require('./queries')

const contacts = async ({ page, limit }) => {
    return await fetch(contactQuery.contacts, page, limit)
}

const contact = async ({ id }) => {
    return await fetchOne(contactQuery.contact, id)
}

const createContact = async ({ phone_number, email }) => {
    return await fetchOne(contactQuery.createContact, phone_number, email);
}

const deleteContact = async ({ id }) => {
    return await fetchOne(contactQuery.deleteContact, id);
}


module.exports = {
    contacts,
    contact,
    createContact,
    deleteContact
}