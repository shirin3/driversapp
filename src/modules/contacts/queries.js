const contacts = `
    select
    *
    from contacts
    order by created_at desc
    offset($1 - 1) * $2 fetch next $2 rows only;
`

const contact = `
    select
    *
    from contacts
    where id = $1
`

const createContact = `
    insert into contacts (
        phone_number,
        email
    ) values (
        $1,
        $2
    )
    returning *
`

const deleteContact = `
    delete 
    from 
    contacts 
    where id = $1
    returning *
`

module.exports = {
    contacts,
    contact,
    createContact,
    deleteContact
}