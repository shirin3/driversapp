const { gql } = require('apollo-server-express')

module.exports = gql`
    type Contact {
        id: ID!
        phone_number: String!
        email: String!
    }

    extend type Query {
        contacts(page: Int limit: Int): [Contact!]!
    }

    extend type Mutation {
        createContact(phone_number: String, email: String): Contact!,
        deleteContact(id: ID!): Contact!
    }
    
`