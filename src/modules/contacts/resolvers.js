const { GraphQLDateTime } = require('graphql-iso-date')
const State = require('./contact')
const checkAuth = require('../../util/checkAuth')
const { AuthenticationError, UserInputError } = require('apollo-server-express')


module.exports = {
    Date: GraphQLDateTime,
    Query: {
        contacts: async (_, args, ctx) => {
            const admin = await checkAuth(ctx)
            if (admin) {
                return await State.contacts(args)
            }
            else {
                throw new AuthenticationError('Not allowed')
            }
        },
    },
    Mutation: {
        createContact: async (_, args, ctx) => {
            const admin = await checkAuth(ctx)
            if (admin) {
                return await State.createContact(args)
            }
            else {
                throw new AuthenticationError('Not allowed')
            }
        },
        deleteContact: async (_, args, ctx) => {
            const admin = await checkAuth(ctx)
            if (admin) {
                return await State.deleteContact(args)
            }
            else {
                throw new AuthenticationError('Not allowed')
            }
        },
    }
}