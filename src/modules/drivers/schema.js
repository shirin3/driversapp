const { gql } = require('apollo-server-express')

module.exports = gql`
    scalar Date
    type Driver {
        id: ID!
        first_name: String!
        last_name: String!
        phone_number: String!
        email: String!
        birthdate: Date!
        security_number: ID!
        address_line_first: String!
        address_line_second: String!
        city: String!
        state: ID!
        zip: ID!
        additional_comment: String
    }

    type Query {
        drivers(page: Int limit: Int): [Driver!]!
    }

    type Mutation {
        createDriver(
            first_name: String!, 
            last_name: String!, 
            phone_number: String!, 
            email: String, 
            birthdate: Date!, 
            security_number: ID!,
            address_line_first: String!,
            address_line_second: String!,
            city: String!,
            state: ID!,
            zip: ID!,
            additional_comment: String
        ): Driver!
    }
    
`