const drivers = `
    select
    *
    from drivers
    order by created_at desc
    offset($1 - 1) * $2 fetch next $2 rows only;
`

const driver = `
    select
    *
    from drivers
    where id = $1
`

const createDriver = `
    insert into drivers (
        first_name,
        last_name,
        phone_number,
        email,
        birthdate,
        security_number,
        address_line_first,
        address_line_second,
        city,
        state,
        zip,
        additional_comment
    ) values (
        $1,
        $2,
        $3,
        $4,
        $5,
        $6,
        $7,
        $8,
        $9,
        $10,
        $11,
        $12
    )
    returning *
`

module.exports = {
    drivers,
    driver,
    createDriver
}