const { fetch, fetchOne } = require('../../library/database/postgres')
const driverQuery = require('./queries')

const drivers = async ({ page, limit }) => {
    return await fetch(driverQuery.drivers, page, limit)
}

const driver = async ({ id }) => {
    return await fetchOne(driverQuery.driver, id)
}

const createDriver = async ({ first_name, last_name, phone_number, email, birthdate, security_number, address_line_first, address_line_second, city, state, zip, additional_comment }) => {
    return await fetchOne(driverQuery.createDriver, first_name, last_name, phone_number, email, birthdate, security_number, address_line_first, address_line_second, city, state, zip, additional_comment);
}


module.exports = {
    drivers,
    driver,
    createDriver
}