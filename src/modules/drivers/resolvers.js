const { GraphQLDateTime } = require('graphql-iso-date')
const Driver = require('./driver')
const checkAuth = require('../../util/checkAuth')
const { AuthenticationError, UserInputError } = require('apollo-server-express')
const { fetchOne } = require('../../library/database/postgres')

const emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
const phoneRegex = /^\(?([0-9]{3})\)?[-.●]?([0-9]{3})[-.●]?([0-9]{4})$/

module.exports = {
    Date: GraphQLDateTime,
    Driver: {
        state: async ({ state }) => {
            return (await fetchOne('select * from states where id = $1', state)).name
        },
    },
    Query: {
        drivers: async (_, args, ctx) => {
            const admin = await checkAuth(ctx)
            if (admin) {
                return await Driver.drivers(args)
            }
            else {
                throw new AuthenticationError('Not allowed')
            }
        },
    },
    Mutation: {
        createDriver: async (_, args, ctx) => {
            const admin = await checkAuth(ctx)
            if (admin) {
                if (
                    args.first_name.trim() === '' ||
                    args.last_name.trim() === '' ||
                    args.phone_number.trim() === '' ||
                    args.email.trim() === '' ||
                    args.birthdate.trim() === '' ||
                    args.security_number.trim() === ''
                ) {
                    throw new UserInputError('Input must not be empty')
                }
                if (!args.phone_number.match(phoneRegex)) {
                    throw new UserInputError('Phone number must be a valid number')
                }
                if (!args.email.match(emailRegex)) {
                    throw new UserInputError('Email must be a valid email address')
                }
                else {
                    return await Driver.createDriver(args)
                }
            }
            else {
                throw new AuthenticationError('Not allowed')
            }
        },
    }
}