const admins = `
    select
    *
    from admins
    order by id desc
    offset($1 - 1) * $2 fetch next $2 rows only;
`

const admin = `
    select
    *
    from admins
    where id = $1
`

const createAdmin = `
    insert into admins (
        username,
        password
    ) values (
        $1,
        crypt($2, gen_salt('bf'))
    )
    returning *
`

const loginAdmin = `
    select 
    *
    from admins 
    where
    username = $1 and password = crypt($2, password)
`

const updateAdmin = `
    update admins
    set
    username = coalesce($2, username),
    password = crypt(coalesce($3, password), gen_salt('bf'))
    where id = $1 
    returning *
`

const deleteAdmin = `
    delete
    from admins
    where id = $1
    returning *
`

module.exports = {
    admins,
    admin,
    createAdmin,
    loginAdmin,
    updateAdmin,
    deleteAdmin
}