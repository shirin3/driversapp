const { gql } = require('apollo-server-express')

module.exports = gql`
    type Admin {
        id: ID!
        username: String!
    }

    type Login {
        admin: Admin!
        token: String!
    }

    extend type Query {
        admins(page: Int limit: Int): [Admin!]!
        admin(id: ID!): Admin!
    }

    extend type Mutation {
        createAdmin(username: String! password: String!): Admin!
        loginAdmin(username: String! password: String!): Login!
        updateAdmin(id: ID! username: String password: String): Admin!
        deleteAdmin(id: ID!): Admin!
    }
`