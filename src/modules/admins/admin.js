const { fetch, fetchOne } = require('../../library/database/postgres')
const adminQuery = require('./queries')
const { SECRET_KEY } = require('../../settings')
const jwt = require('jsonwebtoken')

const admins = async ({ page, limit }) => {
    return await fetch(adminQuery.admins, page, limit)
}

const admin = async ({ id }) => {
    return await fetchOne(adminQuery.admin, id)
}

const createAdmin = async ({ username, password }) => {
    return await fetchOne(adminQuery.createAdmin, username, password);
}

const loginAdmin = async ({ username, password }) => {
    const admin = await fetchOne(adminQuery.loginAdmin, username, password);
    const token = jwt.sign(admin, SECRET_KEY, { expiresIn: "24h" });
    return {
        admin,
        token
    };
}

const updateAdmin = async ({ id, username, password }) => {
    return await fetchOne(adminQuery.updateAdmin, id, username, password);
}

const deleteAdmin = async ({ id }) => {
    return await fetchOne(adminQuery.deleteAdmin, id)
}

module.exports = {
    admins,
    admin,
    createAdmin,
    loginAdmin,
    updateAdmin,
    deleteAdmin
}