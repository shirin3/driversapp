const Admin = require('./admin')
const checkAuth = require('../../util/checkAuth')
const { AuthenticationError, UserInputError } = require('apollo-server-express')

module.exports = {
    Query: {
        admins: async (_, args, ctx) => {
            const admin = await checkAuth(ctx)
            if (admin.admin_id) {
                return await Admin.admins(args)
            }
            else {
                throw new AuthenticationError('Not allowed')
            }
        },
        admin: async (_, args, ctx) => {
            const admin = await checkAuth(ctx)
            if (admin.admin_id) {
                return await Admin.admin(args)
            }
            else {
                throw new AuthenticationError('Not allowed')
            }

        }
    },
    Mutation: {
        loginAdmin: (_, args) => Admin.loginAdmin(args),
        createAdmin: async (_, args, ctx) => {
            const admin = await checkAuth(ctx)
            if (admin) {
                if (args.username.trim() === '' || args.password.trim() === '') {
                    throw new UserInputError('Input must not be empty')
                }
                else {
                    return await Admin.createAdmin(args)
                }
            }
            else {
                throw new AuthenticationError('Not allowed')
            }
        },
        updateAdmin: async (_, args, ctx) => {
            const admin = await checkAuth(ctx)
            if (admin) {
                return await Admin.updateAdmin(args)
            }
            else {
                throw new AuthenticationError('Not allowed')
            }
        },
        deleteAdmin: async (_, args, ctx) => {
            const admin = await checkAuth(ctx)
            if (admin) {
                return await Admin.deleteAdmin(args)
            }
            else {
                throw new AuthenticationError('Not allowed')
            }
        }
    }
}