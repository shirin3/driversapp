const { gql } = require('apollo-server-express')

module.exports = gql`
    type State {
        id: ID!
        name: String!
        is_active: Boolean!
    }

    extend type Query {
        states(page: Int limit: Int): [State!]!
    }

    extend type Mutation {
        createState(name: String!): State!,
        updateState(id: ID!, name: String, is_active: Boolean): State!,
        deleteState(id: ID): State!
    }
    
`