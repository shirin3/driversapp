const { GraphQLDateTime } = require('graphql-iso-date')
const State = require('./state')
const checkAuth = require('../../util/checkAuth')
const { AuthenticationError, UserInputError } = require('apollo-server-express')


module.exports = {
    Date: GraphQLDateTime,
    Query: {
        states: async (_, args, ctx) => {
            const admin = await checkAuth(ctx)
            if (admin) {
                return await State.states(args)
            }
            else {
                throw new AuthenticationError('Not allowed')
            }
        },
    },
    Mutation: {
        createState: async (_, args, ctx) => {
            const admin = await checkAuth(ctx)
            if (admin) {
                return await State.createState(args)
            }
            else {
                throw new AuthenticationError('Not allowed')
            }
        },
        updateState: async (_, args, ctx) => {
            const admin = await checkAuth(ctx)
            if (admin) {
                return await State.updateState(args)
            }
            else {
                throw new AuthenticationError('Not allowed')
            }
        },
        deleteState: async (_, args, ctx) => {
            const admin = await checkAuth(ctx)
            if (admin) {
                return await State.deleteState(args)
            }
            else {
                throw new AuthenticationError('Not allowed')
            }
        },
    }
}