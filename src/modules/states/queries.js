const states = `
    select
    *
    from states
    order by created_at desc
    offset($1 - 1) * $2 fetch next $2 rows only;
`

const state = `
    select
    *
    from states
    where id = $1
`

const createState = `
    insert into states (
        name
    ) values (
        $1
    )
    returning *
`

const updateState = `
    update states 
    set
    name = coalesce($2, name),
    is_active = coalesce($3, is_active)
    where id = $1
    returning *
`

const deleteState = `
    delete from states 
    where id = $1
    returning *
`

module.exports = {
    states,
    state,
    createState,
    updateState,
    deleteState
}