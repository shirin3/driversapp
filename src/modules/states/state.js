const { fetch, fetchOne } = require('../../library/database/postgres')
const stateQuery = require('./queries')

const states = async ({ page, limit }) => {
    return await fetch(stateQuery.states, page, limit)
}

const state = async ({ id }) => {
    return await fetchOne(stateQuery.state, id)
}

const createState = async ({ name }) => {
    return await fetchOne(stateQuery.createState, name);
}

const updateState = async ({ id, name, is_active }) => {
    return await fetchOne(stateQuery.updateState, id, name, is_active);
}

const deleteState = async ({ id }) => {
    return await fetchOne(stateQuery.deleteState, id);
}


module.exports = {
    states,
    state,
    createState,
    updateState,
    deleteState
}