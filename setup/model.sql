create extension pgcrypto;

create table states (
    id serial not null primary key,
    name varchar(32) not null,
    is_active boolean default true,
    created_at timestamp with time zone default current_timestamp
);

create table drivers (
    id serial not null primary key,
    first_name varchar(64) not null,
    last_name varchar(64) not null,
    phone_number varchar(15) not null,
    email varchar(254) not null,
    birthdate date not null,
    security_number varchar(60) not null,
    address_line_first  varchar(508) not null,
    address_line_second  varchar(508),
    city varchar(60) not null,
    state int not null references states(id),
    zip varchar(60) not null,
    additional_comment text,
    created_at timestamp with time zone default current_timestamp
);

create table admins (
    id serial not null primary key,
    username varchar(64) not null,
    password varchar(60) not null,
    created_at timestamptz not null default current_timestamp
);

create table contacts (
    id serial not null primary key,
    email varchar(254) not null,
    phone_number varchar(15) not null,
    created_at timestamptz not null default current_timestamp
);